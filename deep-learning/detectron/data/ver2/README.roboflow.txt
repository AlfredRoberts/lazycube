
LazyCube - v3 S1V2 - No Augmentation
==============================

This dataset was exported via roboflow.com on November 10, 2022 at 10:17 PM GMT

Roboflow is an end-to-end computer vision platform that helps you
* collaborate with your team on computer vision projects
* collect & organize images
* understand unstructured image data
* annotate, and create datasets
* export, train, and deploy computer vision models
* use active learning to improve your dataset over time

It includes 123 images.
Rubiks-Cube-Squares are annotated in COCO format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 400x400 (Fit within)

No image augmentation techniques were applied.


