package com.uon.lazycube;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.uon.lazycubeapp.R;

/**
 * Tutorial fragment template code to populate with the
 * tutorial layout, to display the user manual to help users
 * use the app.
 */
public class TutorialFragment extends Fragment {
    public TutorialFragment() {
            super(R.layout.fragment_tutorial);
        }

    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }
}
