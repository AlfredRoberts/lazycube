<div align="center">
    <img src="docs/uon.png"><br>
    <img height="20%" src="docs/banner.png" />
    <br>
    <a href="https://developer.android.com/studio">
    <img src="https://img.shields.io/badge/Android-3DDC84?style=flat&logo=android&logoColor=white"/></a>
    <a href="https://opensource.org/licenses/Apache-2.0"><img src="https://img.shields.io/badge/License-Apache_2.0-blue.svg"/></a>
    <a href="https://gitlab.com/AlfredRoberts/lazycube/-/wikis/home"><img src="https://img.shields.io/badge/docs-development-blue"></a>
</div>

## Description

**Nottingham Universitry Computer Science Group project by Team 43**

An automatic rubik's cube scanning and solving Android app, powered by TensorFlow.

<div align="center">
    <img width="25%" src="docs/scanning.jpg"/>
    <img width="25%" src="docs/solving.jpg"/>
    <img width="25%" src="docs/complete.jpg"/>
</div>


The project is split into three main components:

- Tensorflow Lite object detection model - see `deep-learning/`
- Internal logic for aligning scanned faces - see `logic/`
- Android UI representation for the project - see `app/`

## Installation

The latest version (v0.4) can be downloaded from [releases](https://gitlab.com/AlfredRoberts/lazycube/-/releases/v0.4).

## Contributing

> Any api or local runners have been de-activated. Automated CI and training scripts no longer work.

See this [document](./CONTRIBUTING.md) for guidelines for contributing in this project.

## Changelog

Changes from major updates (sprint deliverables) can be found in the [changelog](./CHANGELOG.md).

## Wiki

Link to the development documentation can be found [here](https://projects.cs.nott.ac.uk/comp2002/2022-2023/team43_project/-/wikis/home).

## Links

- [Current Public Data set](https://universe.roboflow.com/lazycube/lazycube) - Development [link](https://app.roboflow.com/lazycube/lazycube)

## Authors

- Alexander Bull
- Alfie Inman
- Alfred Roberts
- Hisham Sohawon
- Matthew Bedford
- Sarwar Rashid
- Tautvydas Zilevicius

## Credits

- [AnimCube](https://github.com/cjurjiu/AnimCubeAndroid) - Used to display the scanned cube in 3D
- [min2phase](https://github.com/cs0x7f/min2phase) - Used for cube solving
- [Cubot.io](https://github.com/AkshathRaghav/cubot.io ) - Simple solver, no longer in production use
