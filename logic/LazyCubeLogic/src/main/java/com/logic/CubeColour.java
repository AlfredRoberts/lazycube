package com.logic;

/**
 * @author Alexander Bull
 * @description Enumerator for each colour on a Rubik's cube
 */
public enum CubeColour {
    ORANGE,
    GREEN,
    RED,
    BLUE,
    WHITE,
    YELLOW,
    NULL,
}
